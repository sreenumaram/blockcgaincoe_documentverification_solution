function generateCertificate() {
    const recipientName = document.getElementById('recipientName').value;
    const eventName = document.getElementById('eventName').value;
  
    // Update certificate content
    document.getElementById('certificateRecipient').textContent = recipientName;
    document.getElementById('certificateEvent').textContent = eventName;
  
    // Show the certificate
    document.getElementById('certificate').style.display = 'block';

    // Show the download button
    const downloadButton = document.getElementById('downloadButton');
    downloadButton.style.display = 'block';
  }
  
  function downloadCertificate() {
    const certificate = document.getElementById('certificate');

    // Convert the certificate div to an image
    html2canvas(certificate).then(function(canvas) {
      // Create a download link
      const downloadLink = document.createElement('a');
      downloadLink.href = canvas.toDataURL('image/png');
      downloadLink.download = 'certificate.png';

      // Append the link to the document and trigger a click to start the download
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    });
  }

  function dataURLtoBlob(dataURL) {
    const byteString = atob(dataURL.split(',')[1]);
    const mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);

    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });
  }


  function dataURLtoFile(dataURL, fileName) {
    const byteString = atob(dataURL.split(',')[1]);
    const mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
  
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
  
    const blob = new Blob([ab], { type: mimeString });
    return new File([blob], fileName, { type: mimeString });
  }
  
  async function UploadCertificate() {
    
    // Convert the certificate div to an image
    const certificate = document.getElementById('certificate');
    const canvas = await html2canvas(certificate);
    const imageDataURL = canvas.toDataURL('image/png');

    // Create a Blob from the data URL
    const blob = dataURLtoBlob(imageDataURL);
    console.log(imageDataURL);


    const file = dataURLtoFile(imageDataURL, 'certificate.png');

// Create FormData and append the file
const formData = new FormData();
formData.append('file', file);

    // Create a FormData object and append the Blob
    // const formData = new FormData();
    // formData.append('file', blob);

    // you can JWT (Secret access token) by creating API key in pinata cloud

const JWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI4YTFkNzMzZS1kNmI4LTRmZWItYmZjYi02Y2ZjYmViZmYxM2YiLCJlbWFpbCI6InNyZWVuaXZhc2EubWFyYW1AcXVhbGl0ZXN0Z3JvdXAuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBpbl9wb2xpY3kiOnsicmVnaW9ucyI6W3siaWQiOiJGUkExIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9LHsiaWQiOiJOWUMxIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9XSwidmVyc2lvbiI6MX0sIm1mYV9lbmFibGVkIjpmYWxzZSwic3RhdHVzIjoiQUNUSVZFIn0sImF1dGhlbnRpY2F0aW9uVHlwZSI6InNjb3BlZEtleSIsInNjb3BlZEtleUtleSI6ImY4NmRkZjhiZTU5YzZmNDlkNjA2Iiwic2NvcGVkS2V5U2VjcmV0IjoiZTk0OTYwMTA4ZGVjMzIwZDI5YzE5NzZlYmU3YzJiZGRlYjA2N2NlNzlkMGRmZDQxOTQyYTE4NzU0ZDhjMDU2ZSIsImlhdCI6MTY5NTI3NzcxNn0.1CfOxJPBYnkAx8HYA7yF3FMk6apSjM4DCFOpwhVzIq8'
let objectName= "certificate.png";

const pinataMetadata = JSON.stringify({ name: objectName });
formData.append('pinataMetadata', pinataMetadata);

const pinataOptions = JSON.stringify({ cidVersion: 0 });
formData.append('pinataOptions', pinataOptions);

try {
const res = await fetch("https://api.pinata.cloud/pinning/pinFileToIPFS", {
method: 'POST',
body: formData,
headers: {
'Authorization': `Bearer ${JWT}`
}
});

if (res.ok) {
const data = await res.json();
console.log(data);
const result = JSON.stringify(data);
result.split()
// const resultContainer = document.getElementById('result_container');
//     resultContainer.textContent = result;
console.log("uploaded successfully");
} else {
console.error('Failed to upload file to IPFS:', res.status, res.statusText);
}
} catch (error) {
console.error('Error:', error);
}
}







  