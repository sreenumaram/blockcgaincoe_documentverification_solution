const JWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI4YTFkNzMzZS1kNmI4LTRmZWItYmZjYi02Y2ZjYmViZmYxM2YiLCJlbWFpbCI6InNyZWVuaXZhc2EubWFyYW1AcXVhbGl0ZXN0Z3JvdXAuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBpbl9wb2xpY3kiOnsicmVnaW9ucyI6W3siaWQiOiJGUkExIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9LHsiaWQiOiJOWUMxIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9XSwidmVyc2lvbiI6MX0sIm1mYV9lbmFibGVkIjpmYWxzZSwic3RhdHVzIjoiQUNUSVZFIn0sImF1dGhlbnRpY2F0aW9uVHlwZSI6InNjb3BlZEtleSIsInNjb3BlZEtleUtleSI6ImY4NmRkZjhiZTU5YzZmNDlkNjA2Iiwic2NvcGVkS2V5U2VjcmV0IjoiZTk0OTYwMTA4ZGVjMzIwZDI5YzE5NzZlYmU3YzJiZGRlYjA2N2NlNzlkMGRmZDQxOTQyYTE4NzU0ZDhjMDU2ZSIsImlhdCI6MTY5NTI3NzcxNn0.1CfOxJPBYnkAx8HYA7yF3FMk6apSjM4DCFOpwhVzIq8'

async function UnpinFileFromIPFS() {   
let resultContainer;
const CID = document.getElementById('textInput').value;
try {
    const res = await fetch(`https://api.pinata.cloud/pinning/unpin/${CID}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${JWT}`,
        'accept': 'application/json'
      }
    })
    if (res.status === 200) {
    resultContainer = document.getElementById('result_container');
    resultContainer.textContent = res.status + `  File with CID ${CID} successfully unpinned.` ;
    } else{
        resultContainer = document.getElementById('result_container');
        resultContainer.textContent = res.status + `  Error:The current user has not pinned the cid: ${CID}`;
    // elseif(res.status === 400){
    //   console.error(`Error:The current user has not pinned the cid: ${CID}`);
    //   console.log(res.statusText);
    }
  }catch (error) {
    console.error('An error occurred:', error);
  }}
  

  