
window.CONTRACT = {
  // address: "0xb0482BDd7e23daF65D38911eb5D0eFc27bd6d87d",
  // network: "HTTP://127.0.0.1:7545",
  // address: "0xb7836C468bC64C729E873f0FA4191FF4b7b9F09b",

  // polygon mumbai
  // address: "0x00450641a3fbf0E2383101132E48096a9Ac673D8",
  // network: "https://polygon-mumbai.g.alchemy.com/v2/WezGAtSX0lq3AhSP8lIRIiftaCYBR3YR",

  
  network: "https://a0mtczpsjd:tFnGKGcYMzhK8Wl9-9L0QupSf5uLW2hlpFTtLDa-0Tk@a0e6abbjr2-a0w67afc0g-rpc.au0-aws.kaleido.io/",
  address: "0xfc9AEb0a17Fd07da392DE06Ad375f84E521D5958",
  explore: "https://mumbai.polygonscan.com/",
  abi:[
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "sender",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "hash",
          "type": "bytes32"
        }
      ],
      "name": "RequestViewDocConfirmation",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "_exporter",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "string",
          "name": "_ipfsHash",
          "type": "string"
        }
      ],
      "name": "addHash",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "hash",
          "type": "bytes32"
        },
        {
          "internalType": "string",
          "name": "_ipfs",
          "type": "string"
        }
      ],
      "name": "addDocHash",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_info",
          "type": "string"
        }
      ],
      "name": "add_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_newInfo",
          "type": "string"
        }
      ],
      "name": "alter_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "changeOwner",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "count_Exporters",
      "outputs": [
        {
          "internalType": "uint16",
          "name": "",
          "type": "uint16"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "count_hashes",
      "outputs": [
        {
          "internalType": "uint16",
          "name": "",
          "type": "uint16"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "_hash",
          "type": "bytes32"
        }
      ],
      "name": "deleteHash",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        }
      ],
      "name": "delete_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "_hash",
          "type": "bytes32"
        }
      ],
      "name": "findDocHash",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        }
      ],
      "name": "getExporterInfo",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    }
  ]
  
};
let currentLoggedInAddress;
let adminMenu = document.getElementById("admin_tab");

async function checkAdmin() {
  let currentURL = window.location.href;
  console.log("Current URL: " + currentURL);
  
  if(currentLoggedInAddress!="0x42dad7efbc3bf9082c630c5a84e54cc0bcb1960a")
  {
    console.log("###ADDRESS###:"+currentLoggedInAddress+"     0x42dad7efbc3bf9082c630c5a84e54cc0bcb1960a");
    $("#admin_tab").hide();
    // adminMenu.style.display="none"
  }
  else{
    $("#admin_tab").show();
  }

  if(currentURL.includes("admin") && currentLoggedInAddress!="0x42dad7efbc3bf9082c630c5a84e54cc0bcb1960a")
  {
     document.getElementById("home_tab").click();
  }
}

// async function checkAdmin() {
//   getAdminInfo();
//   let admin1;
//   let admin2;
//   let admin3;
//   $("div.transactions").hide();
//   await window.contract.methods
//     .getOwner()
//     .call({ from: window.userAddress })
//     .then((result) => {
//       admin1 = result;
//     }) ;
    
//   await window.contract.methods
//   .getUserX()
//   .call({ from: window.userAddress })
//   .then((result) => {
//     admin2 = result;
//   }) 
  
//   await window.contract.methods
//   .getUserY()
//   .call({ from: window.userAddress })
//   .then((result) => {
//     admin3 = result;
//   }) 

//     let currentURL = window.location.href;
//   console.log("Current URL: " + currentURL);
//   console.log("Current login address: "+currentLoggedInAddress );
//   console.log("Admin1: "+ admin1.toLowerCase());
//   console.log("Admin2: "+ admin2.toLowerCase());
//   console.log("Admin3: "+ admin3.toLowerCase());
//   if(currentLoggedInAddress!= admin1.toLowerCase() && currentLoggedInAddress!= admin2.toLowerCase() &&currentLoggedInAddress!= admin3.toLowerCase() )
//   {
//     console.log("###ADDRESS###:"+currentLoggedInAddress+"     0x42dad7efbc3bf9082c630c5a84e54cc0bcb1960a");
//     $("#admin_tab").hide();
//     // adminMenu.style.display="none"
//   }
//   else{
//     $("#admin_tab").show();
//   }

//   if(currentURL.includes("admin") &&  (currentLoggedInAddress!= admin1.toLowerCase() && currentLoggedInAddress!= admin2.toLowerCase() &&currentLoggedInAddress!= admin3.toLowerCase() ) )
//   {
//      document.getElementById("home_tab").click();
//   }
// }

async function connect() {
  
  if (window.ethereum) {
    try {
      const selectedAccount = await window.ethereum
        .request({
          method: "eth_requestAccounts",
        })
        .then((accounts) => {
          return accounts[0];
        })
        .catch(() => {
          throw Error("No account selected 👍");
        });
       
      window.userAddress = selectedAccount;
      currentAddress=selectedAccount
      console.log(selectedAccount);
      window.localStorage.setItem("userAddress", window.userAddress);
      
       window.location.reload();
       
     
     
  
    } catch (error) {
      if((error.message).includes("No account selected")){
        alert('No account selected');
      }else{
        alert('Please unlock your wallet');

      }
      
     
      
    }
  } else {
    $("#upload_file_button").attr("disabled", true);
    $("#doc-file").attr("disabled", true);
    // Show The Warning for not detecting wallet
    document.querySelector(".alert").classList.remove("d-none");
  }
}


window.onload = async () => {
  $("#loader").hide();
  $("#loginButton").hide();
  $("#recent-header").hide();
  $(".loader-wraper").fadeOut("slow");
  hide_txInfo();
  $("#upload_file_button").attr("disabled", true);
  
  window.userAddress = window.localStorage.getItem("userAddress");

  if (window.ethereum) {
    window.web3 = new Web3(window.ethereum);
    window.contract = new window.web3.eth.Contract(
      window.CONTRACT.abi,
      window.CONTRACT.address
    );
  
    if (window.userAddress.length > 10) {
      // let isLocked =await window.ethereum._metamask.isUnlocked();
      //  if(!isLocked) {
      //   disconnect();

      // }
      $("#logoutButton").show();
      $("#loginButton").hide();
      $("#userAddress")
        .html(`<i class="fa-solid fa-address-card mx-2 text-primary"></i>${(
        window.userAddress
      )}
       <a class="text-info" href="${window.CONTRACT.explore}/address/${
        window.userAddress
      }" target="_blank" rel="noopener noreferrer"><i class="fa-solid fa-square-arrow-up-right text-warning"></i></a>  
       </a>`);
       
      currentLoggedInAddress = window.userAddress;
      console.log("current logged in address:"+ currentLoggedInAddress);
      checkAdmin();

      if (window.location.pathname == "/admin.html") await getCounters();

      await getExporterInfo();
      await get_ChainID();
      await get_ethBalance();
      $("#Exporter-info").html(
        `<i class="fa-solid fa-building-columns mx-2 text-warning"></i>${window.info}`
      );

      setTimeout(() => {
        listen();
      }, 0);
    } else {
      $("#logoutButton").hide();
      $("#loginButton").show();
      $("#upload_file_button").attr("disabled", true);
      $("#doc-file").attr("disabled", true);
      $(".box").addClass("d-none");
      $(".loading-tx").addClass("d-none");
    }
  } else {
    //No metamask detected
    $("#logoutButton").hide();
    $("#loginButton").hide();
    $(".box").addClass("d-none");
    $("#upload_file_button").attr("disabled", true);
    $("#doc-file").attr("disabled", true);
    document.querySelector(".alert").classList.remove("d-none");

    // alert("Please download metamask extension first.\nhttps://metamask.io/download/");
    // window.location = "https://metamask.io/download/"
  }
//   window.addEventListener('load', function (event) {
//     // Execute the checkConditions function when the page is refreshed
//     checkAdmin();
// });
};


// window.addEventListener('unload', async function (event) {
//   // Display a confirmation message
//  this.alert("are you sure?")
// disconnect();
//   return confirmationMessage; // For some older browsers
// });


function hide_txInfo() {
  $(".transaction-status").addClass("d-none");
}

function show_txInfo() {
  $(".transaction-status").removeClass("d-none");
}
async function get_ethBalance() {
  await web3.eth.getBalance(window.userAddress, function (err, balance) {
    if (err === null) {
      $("#userBalance").html(
        "<i class='fa-brands fa-gg-circle mx-2 text-danger'></i>" +
          web3.utils.fromWei(balance).substr(0, 6) +
          ""
      );
    } else $("#userBalance").html("n/a");
  });
}

if (window.ethereum) {
  window.ethereum.on("accountsChanged", function (accounts) {
    connect();
  });
}

function printUploadInfo(result) {
  $("#transaction-hash").html(
    // `<a target="_blank" title="View Transaction" href="${window.CONTRACT.explore}/tx/` +
    `<a target="_blank"  href="${window.CONTRACT.explore}/tx/` +
      result.transactionHash +
      '"+><i class="fa fa-check-circle font-size-2 mx-1 text-white mx-1"></i></a>' +
   (result.transactionHash)
  );
  $("#file-hash").html(
    `<i class="fa-solid fa-hashtag mx-1"></i> ${(
      window.hashedfile
    )}`
  );
  $("#contract-address").html(
    `<i class="fa-solid fa-file-contract mx-1"></i> ${
      result.to
    }`
  );
  $("#time-stamps").html('<i class="fa-solid fa-clock mx-1"></i>' + getTime());
  $("#blockNumber").html(
    `<i class="fa-solid fa-link mx-1"></i>${result.blockNumber}`
  );
  $("#blockHash").html(
    `<i class="fa-solid fa-shield mx-1"></i> ${truncateAddress(
      result.blockHash
    )}`
  );
  $("#to-netowrk").html(
    `<i class="fa-solid fa-chart-network"></i> ${window.chainID}`
  );
  $("#to-netowrk").hide();
  $("#gas-used").html(
    `<i class="fa-solid fa-gas-pump mx-1"></i> ${result.gasUsed} Gwei`
  );
  $("#loader").addClass("d-none");
  $("#upload_file_button").addClass("d-block");
  show_txInfo();
  get_ethBalance();

  $("#note").html(`<h5 class="text-info">
   Transaction Confirmed to the BlockChain 😊<i class="mx-2 text-info fa fa-check-circle" aria-hidden="true"></i>
   </h5>`);
  listen();
}

let file;
const JWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI4YTFkNzMzZS1kNmI4LTRmZWItYmZjYi02Y2ZjYmViZmYxM2YiLCJlbWFpbCI6InNyZWVuaXZhc2EubWFyYW1AcXVhbGl0ZXN0Z3JvdXAuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBpbl9wb2xpY3kiOnsicmVnaW9ucyI6W3siaWQiOiJGUkExIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9LHsiaWQiOiJOWUMxIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9XSwidmVyc2lvbiI6MX0sIm1mYV9lbmFibGVkIjpmYWxzZSwic3RhdHVzIjoiQUNUSVZFIn0sImF1dGhlbnRpY2F0aW9uVHlwZSI6InNjb3BlZEtleSIsInNjb3BlZEtleUtleSI6ImY4NmRkZjhiZTU5YzZmNDlkNjA2Iiwic2NvcGVkS2V5U2VjcmV0IjoiZTk0OTYwMTA4ZGVjMzIwZDI5YzE5NzZlYmU3YzJiZGRlYjA2N2NlNzlkMGRmZDQxOTQyYTE4NzU0ZDhjMDU2ZSIsImlhdCI6MTY5NTI3NzcxNn0.1CfOxJPBYnkAx8HYA7yF3FMk6apSjM4DCFOpwhVzIq8'
let objectName;

// async function EnterJWT(){
//     JWT = document.getElementById('textInput').value;
// }

async function UploadFileToIPFS() {
  const formData = new FormData();
file = document.getElementById("doc-file").files[0];
formData.append('file', file);
console.log(file.name);
// objectName = document.getElementById('object_name').value;
objectName = file.name;
const pinataMetadata = JSON.stringify({ name: objectName });
console.log(objectName);
console.log(file);
formData.append('pinataMetadata', pinataMetadata);

const pinataOptions = JSON.stringify({ cidVersion: 0 });
formData.append('pinataOptions', pinataOptions);

try {
const res = await fetch("https://api.pinata.cloud/pinning/pinFileToIPFS", {
method: 'POST',
body: formData,
headers: {
'Authorization': `Bearer ${JWT}`
}
});

if (res.ok) {
const data = await res.json();
console.log(data);
const result = JSON.stringify(data);
result.split()
const resultContainer = document.getElementById('result_container');
    resultContainer.textContent = result;
} else {
console.error('Failed to upload file to IPFS:', res.status, res.statusText);
}
} catch (error) {
console.error('Error:', error);
}
}


let fileCID;
let fileHash;
async function sendHash() {
  file = document.getElementById("doc-file").files[0];

 

        // Check the file type based on the extension
        const fileType = getFileType(file.name);

        // Process the file based on its type
        if (fileType !=='image'  && fileType !== 'pdf') {
          alert('Unsupported file type. Please select an image (jpg, jpeg, png) or a PDF.');
          // window.location.reload();
          return;
        } 
  $("#loader").removeClass("d-none");
  $("#upload_file_button").slideUp();
  $("#note").html(
    `<h5 class="text-info">Please confirm the transaction 🙂</h5>`
  );
  $("#upload_file_button").attr("disabled", true);
  get_ChainID();
  // Initilize Ipfs

  // file = document.getElementById("doc-file").files[0];

 

  //       // Check the file type based on the extension
  //       const fileType = getFileType(file.name);

  //       // Process the file based on its type
  //       if (fileType != 'image'  || fileType != 'pdf') {
  //         alert('Unsupported file type. Please select an image (jpg, jpeg, png) or a PDF.');
  //         window.location.reload();
  //       } 
            
        
  node = await Ipfs.create({ repo: "Ali-ok" + Math.random() });
  const fileReader = new FileReader();
  fileReader.readAsArrayBuffer(file);
  fileReader.onload = async (event) => {
    let result = await node.add(fileReader.result);
    window.ipfsCid = result.path;
  
    MyCID = window.ipfsCid + "/";
    console.log("My-CID 1: " + MyCID);
    fileCID = window.ipfsCid;
    // console.log("&&&&&&&&&&&&&&&&&"+fileCID);
  };
 
let CID;
  // =================================================
  if (window.hashedfile) {
    fileHash = window.hashedfile;
    const file = document.getElementById("doc-file").files[0];
    node = await Ipfs.create({ repo: "Ali-ok" + Math.random() });
    const fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = async (event) => {
      let result = await node.add(fileReader.result);
      window.ipfsCid = result.path;

      contract.methods
      .findDocHash(window.hashedfile)
      .call({ from: window.userAddress })
      .then((result) => {
        console.log(result);
        console.log(result[3])
        CID= result[3];})
    };
    await window.contract.methods
      .addDocHash(window.hashedfile, window.ipfsCid)
      .send({ from: window.userAddress })
      .on("transactionHash", function (_hash) {
        $("#note").html(
          `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined...</h5>`
        );
      })
      .on("receipt", function (receipt) {
        // document.getElementById("uploaded-student-document").src ="https://ipfs.io/ipfs/" + MyCID;
        printUploadInfo(receipt);
        generateQRCode();
        console.log("Image CID:"+  MyCID);
        fileCID = MyCID;
         
        // displaySelectedImage()
      })
      .on("confirmation", function (confirmationNr) {})
      .on("error", function (error) {
        console.log(error.message);
        console.log(window.userAddress );
         if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
          $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
        } 
        else if((error.message).includes("The requested account and/or method has not been authorized by the user"))
        {
          console.log(error.message)
          $("#note").html(`<h5 class="text-center">Please unlock your metamask </h5>`);
          }
        else if(currentExporterInfo=== ""){
          window.info = "Unregistered companies are restricted from uploading files." ;
          $("#note").html(`<h5 class="text-center">Unregistered companies are restricted from uploading files</h5>`);
        }else if(CID == fileCID){
          
          $("#note").html(`<h5 class="text-center">Duplicate File 😏</h5>`);
        }
        // else if((error.message).includes("less than the block's baseFeePerGas"))
        else
        {
          $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
          console.log(error.message);
        }
        
        $("#loader").addClass("d-none");
        $("#upload_file_button").slideDown();
      });
  }
}

let CID;
let fileStateVariable;
async function deleteHash() {
  $("#loader").removeClass("d-none");
  $("#upload_file_button").slideUp();
  $("#note").html(
    `<h5 class="text-info">Please confirm the transaction 🙂</h5>`
  );
  $("#upload_file_button").attr("disabled", true);
  get_ChainID();

  if (window.hashedfile) {

    contract.methods
    .findDocHash(window.hashedfile)
    .call({ from: window.userAddress })
    .then((result) => {
      console.log(result);
      console.log(result[3])
      fileStateVariable= result[0];
      console.log(fileStateVariable);
      CID= result[3];
      console.log("DELETE CID:"+CID);
      $(".transaction-status").removeClass("d-none");
      window.newHash = result;
    })

    await window.contract.methods
      .deleteHash(window.hashedfile)
      .send({ from: window.userAddress })
      .on("transactionHash", function (hash) {
        $("#note").html(
          `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴</h5>`
        );

        //  contract.methods
        // .findDocHash(window.hashedfile)
        // .call({ from: window.userAddress })
        // .then((result) => {
        //   console.log(result);
        //   console.log(result[3])
        //   CID= result[3];
        //   console.log("DELETE CID:"+CID);
        //   $(".transaction-status").removeClass("d-none");
        //   window.newHash = result;
        // })
      })
    .on("receipt", function (receipt) {
      UnpinFileFromIPFS(CID);
        $("#note").html(
          `<h5 class="text-info p-1 text-center">File Deleted Successfully 🙂</h5>`
        );

        $("#loader").addClass("d-none");
        $("#upload_file_button").slideDown();
      })
      .on("confirmation", function (confirmationNr) {
        console.log("My Confiramation : "+confirmationNr);
        console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  : "+ CID);
        
      })
      .on("error", function (error) {
        console.log("$$$$$$$$$$$$$   CID : "+ CID);
        console.log(error.message);
        if(((error.message).includes("The requested account and/or method has not been authorized by the user"))){
          $("#note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
          console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  : "+ CID);
        }
        else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
          $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
        }
        else if(currentExporterInfo=== ""){
          window.info = "Unregistered companies are restricted from deleting files." ;
          $("#note").html(`<h5 class="text-center">Unregistered companies are restricted from deleting files</h5>`);
        }
        else if(fileStateVariable == 0 ){
          console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$  : "+ CID);
        $("#note").html(`<h5 class="text-center">File is not available in the system 😕</h5>`);
        }
        else if((error.message).includes("less than the block's baseFeePerGas") || (((error.message).includes("Internal JSON-RPC error")) && CID != ""))
        {
          $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
        }  
        
        // $("#note").html(`<h5 class="text-center">Document not found 😏</h5>`);
        $("#loader").addClass("d-none");
        $("#upload_file_button").slideDown();
      });
  }
  // UnpinFileFromIPFS(CID);
}
// console.log("Delete CID:" + CID);

async function UnpinFileFromIPFS(CID) {   
let resultContainer;
// const CID = document.getElementById('textInput').value;
try {
    const res = await fetch(`https://api.pinata.cloud/pinning/unpin/${CID}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${JWT}`,
        'accept': 'application/json'
      }
    })
    if (res.status === 200) {
    resultContainer = document.getElementById('result_container');
    resultContainer.textContent = res.status + `  File with CID ${CID} successfully unpinned.` ;
    } else{
        resultContainer = document.getElementById('result_container');
        resultContainer.textContent = res.status + `  Error:The current user has not pinned the cid: ${CID}`;
    // elseif(res.status === 400){
    //   console.error(`Error:The current user has not pinned the cid: ${CID}`);
    //   console.log(res.statusText);
    }
  }catch (error) {
    console.error('An error occurred:', error);
  }}
  

function getTime() {
  let d = new Date();
  a =
    d.getFullYear() +
    "-" +
    (d.getMonth() + 1) +
    "-" +
    d.getDate() +
    " - " +
    d.getHours() +
    ":" +
    d.getMinutes() +
    ":" +
    d.getSeconds();
  return a;
}

async function get_ChainID() {
  let a = await web3.eth.getChainId();
  console.log(a);
  switch (a) {
    case 1:
      window.chainID = "Ethereum Main Network (Mainnet)";
      break;
    case 80001:
      window.chainID = "Polygon Test Network";
      break;
    case 137:
      window.chainID = "Polygon Mainnet";
      break;
    case 3:
      window.chainID = "Ropsten Test Network";
      break;
    case 4:
      window.chainID = "Rinkeby Test Network";
      break;
    case 5:
      window.chainID = "Goerli Test Network";
      break;
    case 42:
      window.chainID = "Kovan Test Network";
      break;
    case  5777:
      window.chainID = "Local Ganache Network";
      break;
    default:
      window.chainID = "Private Network";
      break;
  }
  let network = document.getElementById("network");
  if (network) {
    document.getElementById(
      "network"
    ).innerHTML = `<i class="text-info fa-solid fa-circle-nodes mx-2"></i>${window.chainID}`;
  }
}

function get_Sha3() {
  hide_txInfo();
  $("#note").html(`<h5 class="text-warning">Hashing Your File 😴...</h5>`);

  $("#upload_file_button").attr("disabled", false);

  console.log("file changed");

  var file = document.getElementById("doc-file").files[0];
  if (file) {
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
      // var SHA256 = new Hashes.SHA256();
      // = SHA256.hex(evt.target.result);
      window.hashedfile = web3.utils.soliditySha3(evt.target.result);
      console.log(`Document Hash : ${window.hashedfile}`);
      // console.log("My-CID 1: " + MyCID);
      
      $("#note").html(
        `<h5 class="text-center text-info">File Hashed  😎 </h5>`
      );
    };
    reader.onerror = function (evt) {
      console.log("error reading file");
    };
  } else {
    window.hashedfile = null;
  }
}

function disconnect() {

  $("#logoutButton").hide();
  $("#loginButton").show();
  window.location.reload();
  window.userAddress = null;
  $(".wallet-status").addClass("d-none");
  window.localStorage.setItem("userAddress", null);
  $("#upload_file_button").addClass("disabled");
                                                               
  document.getElementById("home_tab").click();
}

function truncateAddress(address) {
  if (!address) {
    return;
  }
  return `${address.substr(0, 7)}...${address.substr(
    address.length - 8,
    address.length
  )}`;
}

async function addAdmin1(){
  const address = document.getElementById("Admin-address").value.trim();
  let valid = WAValidator.validate(address, 'ETH');
  let admin1;
  let admin2;
  let admin3;
  await window.contract.methods
    .getOwner()
    .call({ from: window.userAddress })
    .then((result) => {
      admin1 = result;
    }) ;
    
  await window.contract.methods
  .getUserX()
  .call({ from: window.userAddress })
  .then((result) => {
    admin2 = result;
  }) 
  
  await window.contract.methods
  .getUserY()
  .call({ from: window.userAddress })
  .then((result) => {
    admin3 = result;
  }) 
 
  if (address) {
    $("#loader").removeClass("d-none");
    $("#Admin1").slideUp();
    $("#Admin2").slideUp();
    $("#Admin3").slideUp();
    
    // $("#Admin1").attr("disabled", true);
    // $("#Admin2").attr("disabled", true);
    // $("#Admin3").attr("disabled", true);
    get_ChainID();
  try {
    // if(window.userAddress.toLowerCase()== admin1.toLowerCase().toLowerCase()){
    //   $("#admin-actions #note").html(`<h5 class="text-center">You cannot place your position</h5>`);
      
    //   $("#Admin1").slideDown();
    //   $("#Admin2").slideDown();
    //   $("#Admin3").slideDown()
    //   return;
      
    // }
    if(valid== false){
      $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
      $("#Admin1").slideDown();
      $("#Admin2").slideDown();
      $("#Admin3").slideDown();
      return;
    }
   else if(address.toLowerCase()== admin1.toLowerCase() || address.toLowerCase()== admin2.toLowerCase() || address.toLowerCase() == admin3.toLowerCase() ){
      $("#admin-actions #note").html(`<h5 class="text-center">Admin Already Exists </h5>`);
      console.log(error.message)
      }
    await window.contract.methods.changeOwner(address)
    .send({ from: window.userAddress })
        .on("transactionHash", function (hash) {
          $("#admin-actions #note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          console.log(receipt);
          $("#admin-actions #note").html(
            `<h5 class="text-info">Admin added successfully</h5>`
          );
        })
        
        .on("confirmation", function (confirmationNr) {})
        .on("error", function (error) {
         if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#admin-actions #note").html(`<h5 class="text-center">${error.message}</h5>`);
          } else if(enteredExporterInfo=== "" || ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
            $("#admin-actions #note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
          } else if((error.message).includes("invalid address")){
            $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
          }else{
            $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
          }
          $("#loader").addClass("d-none");
        
        });

  }catch(error){
    if((error.message).includes("invalid address"))
      {
        $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
      }
      // else{
      //   $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
      // }
      $("#loader").addClass("d-none");
      $("#Admin1").slideDown();
      $("#Admin2").slideDown();
      $("#Admin3").slideDown();
    }
    
  }
else{
  $("#admin-actions #note").html(
    `<h5 class="text-center text-warning">You need to provide address </h5>`
);
}

}


async function addAdmin2(){
  const address = document.getElementById("Admin-address").value.trim();
  let valid = WAValidator.validate(address, 'ETH');
  let admin1;
  let admin2;
  let admin3;
  await window.contract.methods
    .getOwner()
    .call({ from: window.userAddress })
    .then((result) => {
      admin1 = result;
    }) ;
    
  await window.contract.methods
  .getUserX()
  .call({ from: window.userAddress })
  .then((result) => {
    admin2 = result;
  }) 
  
  await window.contract.methods
  .getUserY()
  .call({ from: window.userAddress })
  .then((result) => {
    admin3 = result;
  }) 
 
  if (address) {
    $("#loader").removeClass("d-none");
    $("#Admin1").slideUp();
    $("#Admin2").slideUp();
    $("#Admin3").slideUp();
    
    // $("#Admin1").attr("disabled", true);
    // $("#Admin2").attr("disabled", true);
    // $("#Admin3").attr("disabled", true);
    get_ChainID();
  try {
    // if(window.userAddress.toLowerCase()== admin2.toLowerCase().toLowerCase()){
    //   $("#admin-actions #note").html(`<h5 class="text-center">You cannot place your position</h5>`);
     
    //   $("#Admin1").slideDown();
    //   $("#Admin2").slideDown();
    //   $("#Admin3").slideDown()
    //   return;

    // }
    if(valid== false){
      $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
      $("#Admin1").slideDown();
        $("#Admin2").slideDown();
        $("#Admin3").slideDown()
      
      return;
    }
    else if(address.toLowerCase()== admin1.toLowerCase() || address.toLowerCase()== admin2.toLowerCase() || address.toLowerCase() == admin3.toLowerCase() ){
      $("#admin-actions #note").html(`<h5 class="text-center">Admin Already Exists </h5>`);
      console.log(error.message)
      }
    await window.contract.methods.setUserX(address)
    .send({ from: window.userAddress })
        .on("transactionHash", function (hash) {
          $("#admin-actions #note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          console.log(receipt);
          $("#admin-actions #note").html(
            `<h5 class="text-info">Admin added successfully</h5>`
          );
        })
        
        .on("confirmation", function (confirmationNr) {})
        .on("error", function (error) {
          if((error.message).includes("less than the block's baseFeePerGas"))
        {
          // $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
            $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
       
        }
          else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#admin-actions #note").html(`<h5 class="text-center">${error.message}</h5>`);
          } else if(enteredExporterInfo=== "" || ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
            $("#admin-actions #note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
          } else if((error.message).includes("invalid address")){
            $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
          }else{
            $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
          }
          $("#loader").addClass("d-none");
        
        });

  }catch(error){
    if((error.message).includes("invalid address"))
      {
        $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
      }
      
      $("#loader").addClass("d-none");
      $("#Admin1").slideDown();
      $("#Admin2").slideDown();
      $("#Admin3").slideDown();
    }
    
  }
else{
  $("#admin-actions #note").html(
    `<h5 class="text-center text-warning">You need to provide address </h5>`
);
}

}


async function addAdmin3(){
  const address = document.getElementById("Admin-address").value.trim();
  let valid = WAValidator.validate(address, 'ETH');
  let admin1;
  let admin2;
  let admin3;
  await window.contract.methods
    .getOwner()
    .call({ from: window.userAddress })
    .then((result) => {
      admin1 = result;
    }) ;
    
  await window.contract.methods
  .getUserX()
  .call({ from: window.userAddress })
  .then((result) => {
    admin2 = result;
  }) 
  
  await window.contract.methods
  .getUserY()
  .call({ from: window.userAddress })
  .then((result) => {
    admin3 = result;
  }) 
 
  if (address) {
    $("#loader").removeClass("d-none");
    $("#Admin1").slideUp();
    $("#Admin2").slideUp();
    $("#Admin3").slideUp();
    
    // $("#Admin1").attr("disabled", true);
    // $("#Admin2").attr("disabled", true);
    // $("#Admin3").attr("disabled", true);
    get_ChainID();
  try {
    // if(window.userAddress.toLowerCase()== admin3.toLowerCase()){
    //   $("#admin-actions #note").html(`<h5 class="text-center">You cannot place your position </h5>`);
      
    //   $("#Admin1").slideDown();
    //   $("#Admin2").slideDown();
    //   $("#Admin3").slideDown()
    //   return;
      
    // }
    if(valid== false){
      $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
      $("#Admin1").slideDown();
      $("#Admin2").slideDown();
      $("#Admin3").slideDown();
      return;
    }
    else if(address.toLowerCase()== admin1.toLowerCase() || address.toLowerCase()== admin2.toLowerCase() || address.toLowerCase() == admin3.toLowerCase() ){
      $("#admin-actions #note").html(`<h5 class="text-center">Admin Already Exists </h5>`);
      console.log(error.message)
      }else{
    await window.contract.methods.setUserY(address)
    .send({ from: window.userAddress })
        .on("transactionHash", function (hash) {
          $("#admin-actions #note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          console.log(receipt);
          $("#admin-actions #note").html(
            `<h5 class="text-info">Admin added successfully</h5>`
          );
        })
        
        .on("confirmation", function (confirmationNr) {})
        .on("error", function (error) {
          if((error.message).includes("less than the block's baseFeePerGas"))
        {
          // $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
            $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
       
        }
          else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#admin-actions #note").html(`<h5 class="text-center">${error.message}</h5>`);
          } else if(enteredExporterInfo=== "" || ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
            $("#admin-actions #note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
          } else if((error.message).includes("invalid address")){
            $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
            console.log("BBBBBBBBBBBBBBBBBBBBBBBBB");
          }else{
            $("#admin-actions #note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
          }
          $("#loader").addClass("d-none");
        
        });
      }

  }catch(error){
    if((error.message).includes("invalid address"))
      {
        $("#admin-actions #note").html(`<h5 class="text-center">Invalid address</h5>`);
        console.log("BBBBBBBBBBBBBBBBBBBBBBBBB");
      }
      
      $("#loader").addClass("d-none");
      $("#Admin1").slideDown();
      $("#Admin2").slideDown();
      $("#Admin3").slideDown();
    }
    
  }
else{
  $("#admin-actions #note").html(
    `<h5 class="text-center text-warning">You need to provide address </h5>`
);
}

}






async function getAdminInfo(){

let admin1;
let admin2;
let admin3;

  await window.contract.methods
    .getOwner()
    .call({ from: window.userAddress })
    .then((result) => {
      admin1= result;
    })

    await window.contract.methods
    .getUserX()
    .call({ from: window.userAddress })
    .then((result) => {
      admin2= result;
    })

    await window.contract.methods
    .getUserY()
    .call({ from: window.userAddress })
    .then((result) => {
      admin3= result;
    })

    $("#admin-info #note1").html(`<h5 class="text-center">Admin1: ${admin1}</h5>`);
    $("#admin-info #note2").html(`<h5 class="text-center">Admin2: ${admin2}</h5>`); 
    $("#admin-info #note3").html(`<h5 class="text-center">Admin3: ${admin3}</h5>`);

}



let currentExporterInfo;
async function getExporterInfo() {
  await window.contract.methods
    .getExporterInfo(window.userAddress)
    .call({ from: window.userAddress })
    .then((result) => {
      // window.info = result;
      console.log(result);
      currentExporterInfo = result;
      if(currentExporterInfo=== ""){
        window.info =window.userAddress+ "   -   Not registered" ;
      }else{
        window.info = result+ " - "+window.userAddress;
        console.log("ExporterINFO:"+ window.info);
      }
      
    });
}


let enteredExporterInfo;
// var WAValidator = require('wallet-address-validator');
async function addExporter() {
  const address = document.getElementById("Exporter-address").value.trim();
  const info = document.getElementById("info").value;
  let valid = WAValidator.validate(address, 'ETH');
 
  if((valid == false) || (address.length < 42 || address.length > 42 && info!=="")){
  $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
  return;
}
  else{
  await window.contract.methods
    .getExporterInfo(address)
    .call({ from: window.userAddress })
    .then((result) => {
      enteredExporterInfo = result;
     
    })
  }

  if (info && address) {
    $("#loader").removeClass("d-none");
    $("#ExporterBtn").slideUp();
    $("#edit").slideUp();
    $("#delete").slideUp();
    $("#note").html(
      `<h5 class="text-info">Please confirm the transaction 👍...</h5>`
    );
    // $("#ExporterBtn").attr("disabled", true);
    // $("#delete").attr("disabled", true);
    // $("#edit").attr("disabled", true);
    get_ChainID();
    console.log(enteredExporterInfo+"^^^^^^^^^^^^^^^^^^");

    try {
      await window.contract.methods
        .add_Exporter(address, info)
        .send({ from: window.userAddress })

        .on("transactionHash", function (hash) {
          $("#note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
          $("#edit").slideDown();
          $("#delete").slideDown();
          console.log(receipt);
          $("#note").html(
            `<h5 class="text-info">Exporter Added to the Blockchain 😇</h5>`
          );
          $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
        })

        .on("confirmation", function (confirmationNr) {})
        .on("error", function (error) {
          if((error.message).includes("less than the block's baseFeePerGas"))
        {
          $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
        }
          else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#note").html(`<h5 class="text-center">${error.message}</h5>`);
          } 
          else if(enteredExporterInfo=== "" && ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
            console.log(error.message)
             $("#note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
           }
           else if( enteredExporterInfo!== ""  && (error.message).includes("Internal JSON-RPC error") ){
            $("#note").html(`<h5 class="text-center">Exporter Already Exists </h5>`);
            console.log("kkkkkk"+error.message)
            }else if((error.message).includes("Internal JSON-RPC error"))
            {
              $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
              // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
            }
           
          else if((error.message).includes("invalid address")){
            $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
          }
          console.log(error.message+"QDVA1");
          // $("#note").html(`<h5 class="text-center">${error.message}</h5>`);
          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
        });
    } catch (error) {
      // $("#note").html(`<h5 class="text-center">${error.message}</h5>`);
      if((error.message).includes("invalid address"))
      {
        $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
        console.log(error.message+"QDVA2");
      }
      $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
    }
  }else {
    $("#note").html(
      `<h5 class="text-center text-warning">You need to provide address & information to add  </h5>`
);
  }
}
// let currentExporterInfo;
// async function getExporterInfo() {
//   await window.contract.methods
//     .getExporterInfo(window.userAddress)
//     .call({ from: window.userAddress })
//     .then((result) => {
//       // window.info = result;
//       console.log(result);
//       currentExporterInfo = result;
//       if(currentExporterInfo=== ""){
//         window.info =window.userAddress+ "   -   Not registered" ;
//       }else{
//         window.info = result+ " - "+window.userAddress;
//         console.log("ExporterINFO:"+ window.info);
//       }
      
//     });
// }

async function getCounters() {
  await window.contract.methods
    .count_Exporters()
    .call({ from: window.userAddress })

    .then((result) => {
      $("#num-exporters").html(
        `<i class="fa-solid fa-building-columns mx-2 text-info"></i>${result}`
      );
    });
  await window.contract.methods
    .count_hashes()
    .call({ from: window.userAddress })

    .then((result) => {
      $("#num-hashes").html(
        `<i class="fa-solid fa-file mx-2 text-warning"></i>${result}`
      );
    });
}

async function editExporter() {
  const address = document.getElementById("Exporter-address").value.trim();
  const info = document.getElementById("info").value;
  let valid = WAValidator.validate(address, 'ETH');
  if((valid == false)||(address.length < 42 || address.length > 42 && info!=="")){
    $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
    return;
  }
    else{
    await window.contract.methods
      .getExporterInfo(address)
      .call({ from: window.userAddress })
      .then((result) => {
        enteredExporterInfo = result;
       
      })
    }

  if (info && address) {
    $("#loader").removeClass("d-none");
    $("#ExporterBtn").slideUp();
    $("#edit").slideUp();
    $("#delete").slideUp();
    $("#note").html(
      `<h5 class="text-info">Please confirm the transaction 😴...</h5>`
    );
    // $("#ExporterBtn").attr("disabled", true);
    get_ChainID();

    try {
      await window.contract.methods
        .alter_Exporter(address, info)
        .send({ from: window.userAddress })

        .on("transactionHash", function (hash) {
          $("#note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😇...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
          console.log(receipt);
          $("#note").html(
            `<h5 class="text-info">Exporter Updated Successfully 😊</h5>`
          );
          $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
        })

        .on("confirmation", function (confirmationNr) {})
        .on("error", function (error) {
          if((error.message).includes("less than the block's baseFeePerGas"))
          {
            $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
            // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
          }
           else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#note").html(`<h5 class="text-center">${error.message}</h5>`);
          }
          else if(enteredExporterInfo == ""){
            $("#note").html(`<h5 class="text-center">Exporter Not Exists </h5>`);
            console.log(error.message)
            }
          else if((error.message).includes("Internal JSON-RPC error")){
            $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
          } else if ( enteredExporterInfo != "" && ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
            $("#note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
          }


          // $("#note").html(`<h5 class="text-center">Exporter Not Exists</h5>`);
          // console.log(error.message);
          // $("#note").html(`<h5 class="text-center">${error.message} 👍</h5>`);
          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
        });
    } catch (error) {
      // $("#note").html(`<h5 class="text-center">${error.message} 👍</h5>`);
      if((error.message).includes("invalid address"))
      {
        $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
      }
      
      $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
    }
  } else {
    $("#note").html(
      `<h5 class="text-center text-warning">You need to provide address & information to update 😵‍💫 </h5>`
    );
  }
}

async function deleteExporter() {
  const address = document.getElementById("Exporter-address").value.trim();
  let valid = WAValidator.validate(address, 'ETH');

  if((valid == false) ||(address.length < 42 || address.length >42)){
    $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
    return;
  }
    else{
    await window.contract.methods
      .getExporterInfo(address)
      .call({ from: window.userAddress })
      .then((result) => {
        enteredExporterInfo = result;
       
      })
    }


  if (address) {
    $("#loader").removeClass("d-none");
    $("#ExporterBtn").slideUp();
    $("#edit").slideUp();
    $("#delete").slideUp();
    $("#note").html(
      `<h5 class="text-info">Please confirm the transaction 😕...</h5>`
    );
    // $("#ExporterBtn").attr("disabled", true);
    get_ChainID();

    try {
      await window.contract.methods
        .delete_Exporter(address)
        .send({ from: window.userAddress })

        .on("transactionHash", function (hash) {
          $("#note").html(
            `<h5 class="text-info p-1 text-center">Please wait for transaction to be mined 😴 ...</h5>`
          );
        })

        .on("receipt", function (receipt) {
          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
          $("#edit").slideDown();
          $("#delete").slideDown();
          console.log(receipt);
          $("#note").html(
            `<h5 class="text-info">Exporter Deleted Successfully 🙂</h5>`
          );
          $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
        })
        .on("error", function (error) {
          // console.log(error.message);
          // $("#note").html(`<h5 class="text-center">${error.message} 🙂</h5>`);
          if((error.message).includes("less than the block's baseFeePerGas"))
          {
            $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas</h5>`);
            // $("#note").html(`<h5 class="text-center">${error.message} 😏</h5>`);
          }
          else if(error.message === "MetaMask Tx Signature: User denied transaction signature."){
            $("#note").html(`<h5 class="text-center">${error.message}</h5>`);
          } 
          else if(enteredExporterInfo == ""){
            $("#note").html(`<h5 class="text-center">Exporter Not Exists </h5>`);
            console.log(error.message)
            }
            else if((error.message).includes("Internal JSON-RPC error")){
              $("#note").html(`<h5 class="text-center">Transaction's maxFeePerGas is less than the block's baseFeePerGas </h5>`);
            } else if ( enteredExporterInfo != "" && ((error.message).includes("The requested account and/or method has not been authorized by the user"))){
              $("#note").html(`<h5 class="text-center">Please unlock your metamask</h5>`);
            }


          $("#loader").addClass("d-none");
          $("#ExporterBtn").slideDown();
          $("#edit").slideDown();
          $("#delete").slideDown();
        });
    } catch (error) {
      // $("#note").html(`<h5 class="text-center">${error.message} 🙂</h5>`);
      if((error.message).includes("invalid address"))
      {
        $("#note").html(`<h5 class="text-center">Invalid address</h5>`);
      }
      $("#loader").addClass("d-none");
      $("#ExporterBtn").slideDown();
      $("#edit").slideDown();
      $("#delete").slideDown();
    }
  } else {
    $("#note").html(
      `<h5 class="text-center text-warning">You need to provide address to delete 👍</h5>`
    );
  }
}
let fileUrl;
let verifyPageURL;

function generateQRCode() {
  document.getElementById("qrcode").innerHTML = "";
  console.log("making qr-code...");
  var qrcode = new QRCode(document.getElementById("qrcode"), {
    colorDark: "#000",
    colorLight: "#fff",
    correctLevel: QRCode.CorrectLevel.H,
  });
  if (!window.hashedfile) return;
  let url = `${window.location.host}/verify.html?hash=${window.hashedfile}`;
  verifyPageURL = url;
  fileUrl = `https://ipfs.io/ipfs/${fileCID}`;
  qrcode.makeCode(url);
  console.log(url);
  const qrcodeFileName = removeRightPartAfterLastDot(document.getElementById("doc-file").files[0].name);
  document.getElementById("download-link").download = qrcodeFileName+" QRcode.png"
  //  "Qualitest Document QR code.png"
    // document.getElementById("doc-file").files[0].name;
    
  document.getElementById("verfiy").href =
    window.location.protocol + "//" + url;

  

  function makeDownload() {
    document.getElementById("download-link").href =
      document.querySelector("#qrcode img").src;
      console.log(document.querySelector("#qrcode img").src);
  }
  setTimeout(makeDownload, 500);
  //  makeDownload();

  $("#imageCanvas").hide();
  const fileInput = document.getElementById('doc-file');
  const file = fileInput.files[0];
        // Check the file type based on the extension
        const fileType = getFileType(file.name);

        // Process the file based on its type
        if (fileType === 'image') {
          addTextToImage()
        } 
 
  
}

function removeRightPartAfterLastDot(inputString) {
  // Find the index of the last dot
  const lastDotIndex = inputString.lastIndexOf('.');

  // Check if a dot was found
  if (lastDotIndex !== -1) {
      // Extract the left part of the string before the last dot
      const result = inputString.substring(0, lastDotIndex);
      return result;
  }

  // Return the original string if no dot is found
  return inputString;
}

// Example usage
const inputString = "example.file.txt";
const result = removeRightPartAfterLastDot(inputString);
console.log(result); // Output: example.file


async function listen() {
  console.log("started...");
  if (window.location.pathname != "/upload.html") return;
  document.querySelector(".loading-tx").classList.remove("d-none");
  window.web3 = new Web3(window.ethereum);
  window.contract = new window.web3.eth.Contract(
    window.CONTRACT.abi,
    window.CONTRACT.address
  );
  await window.contract.getPastEvents(
    "addHash",
    {
      filter: {
        _exporter: window.userAddress, //Only get the documents uploaded by current Exporter
      },
      fromBlock: (await window.web3.eth.getBlockNumber()) - 999,
      toBlock: "latest",
    },
    function (error, events) {
      printTransactions(events);
      console.log(events);
    }
  );
}

function printTransactions(data) {
  document.querySelector(".transactions").innerHTML = "";
  document.querySelector(".loading-tx").classList.add("d-none");
  if (!data.length) {
    $("#recent-header").hide();
    return;
  }
  $("#recent-header").show();
  const main = document.querySelector(".transactions");
  for (let i = 0; i < data.length; i++) {
    const a = document.createElement("a");
    a.href = `${window.CONTRACT.explore}` + "/tx/" + data[i].transactionHash;
    a.setAttribute("target", "_blank");
    a.className =
      "col-lg-3 col-md-4 col-sm-5 m-2  bg-dark text-light rounded position-relative card";
    a.style = "overflow:hidden;";
    const image = document.createElement("object");
    image.style = "width:100%;height: 100%;";
    image.data = `https://ipfs.io/ipfs/${data[i].returnValues[1]}`;
    // image.data = `https://u0iiq4qpdw-u0iwba4m20-ipfs.us0-aws.kaleido.io/ipfs/${data[i].returnValues[1]}`;
    const num = document.createElement("h1");
    num.append(document.createTextNode(i + 1));
    a.appendChild(image);
    num.style =
      "position:absolute; left:4px; bottom: -20px;font-size:4rem; color: rgba(20, 63, 74, 0.35);";
    a.appendChild(num);
    main.prepend(a);
  }
}






function addTextToPDF() {
  const fileInput = document.getElementById('doc-file');
  const file = fileInput.files[0];

  const fileReader = new FileReader();

  fileReader.onload = async function(event) {
    const pdfBytes = event.target.result;
    const pdfDoc = await PDFLib.PDFDocument.load(pdfBytes);

    const firstPage = pdfDoc.getPages()[0];
    const { width, height } = firstPage.getSize();

    const font = await pdfDoc.embedFont(PDFLib.StandardFonts.Helvetica);

    const resultedFileUrl = "File URL: "+fileUrl;
    const resultedFileHash = "File hash: "+fileHash;
    const urlToVerify = "Verify page URL: " +"http://"+verifyPageURL

  //  const ww = width.toString();
  //  const hh = height.toString();
  //  console.log("width:"+ww);
  //  console.log("height:"+hh);

   
    const xPercentage = 5; // Adjust this percentage as needed
    const yPercentage = 13.5;
    const fontSizePercentage = 1.5;

     const xRelativeToWidth = (width * xPercentage) / 100;
     const yRelativetoWidth =  (height * yPercentage) / 100;
     const calculatedFontSize = (fontSizePercentage * width) / 100;



    firstPage.drawText(resultedFileHash, {
      x: xRelativeToWidth,
      // y: height - 60,
      // y: calculatedFontSize *80,
      y: yRelativetoWidth,
      font,
      size: 8,
      color: PDFLib.rgb(0, 0, 0), // Black color
    });

    firstPage.drawText(resultedFileUrl, {
      x: xRelativeToWidth,
      y: yRelativetoWidth - calculatedFontSize * 1.5,
      font,
      size: 8,
      color: PDFLib.rgb(0, 0, 0), // Black color
    });

    firstPage.drawText(urlToVerify, {
      x: xRelativeToWidth,
      y: yRelativetoWidth - calculatedFontSize * 3,
      font,
      size: 8,
      color: PDFLib.rgb(0, 0, 0), // Black color
    });

 

    const modifiedPdfBytes = await pdfDoc.save();
    const modifiedPdfBlob = new Blob([modifiedPdfBytes], { type: 'application/pdf' });

    const downloadLink = document.createElement('a');
    downloadLink.href = URL.createObjectURL(modifiedPdfBlob);
    downloadLink.download = removeRightPartAfterLastDot(document.getElementById("doc-file").files[0].name)+" Updated.pdf";
    downloadLink.click();
  };

  fileReader.readAsArrayBuffer(file);
}





// To add text on image

async function addTextToImage() {
  $("#uploaded-image").html(
    `<canvas id="imageCanvas"  width="841.92" height="595.499" style="border:1px solid #000  "></canvas>`
  );
  const imageInput = document.getElementById('doc-file');
  const canvas = document.getElementById('imageCanvas');
  const ctx = canvas.getContext('2d');

  // Check if a file is selected
  if (imageInput.files.length === 0) {
      alert('Please select an image file.');
      return;
  }

  const file = imageInput.files[0];
  const imgUrl = URL.createObjectURL(file);

  // Load the image
  const img = new Image();
  img.src = imgUrl;

  img.onload = function() {
      // Draw the image on the canvas
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

      // Add text to the canvas with word wrapping
      const hashCode =  "File hash: "+fileHash;
      const ipfsCID =  "File URL: "+fileUrl;
      const x = 35; // X position of the text
      const y1 = 520; // Y position of the text
      const y2 = y1+20;
      const fontSize = 10;
      const maxWidth = 600; // Maximum width for word wrapping
      
      ctx.font = `${fontSize}px Arial`;
      ctx.fillStyle = 'Black'; // Text color

      wrapText(ctx, hashCode, x, y1, maxWidth, fontSize);
      wrapText(ctx, ipfsCID, x, y2, maxWidth, fontSize);

     
      
    
      document.getElementById("imageCanvas").innerHTML = "";
      let qrcode = new QRCode(document.getElementById("imageCanvas"), {
        colorDark: "#000",
        colorLight: "#fff",
        correctLevel: QRCode.CorrectLevel.H,
        width :100,
        height:100
      });

      qrcode.makeCode(verifyPageURL);
      ctx.drawImage(qrcode._el.firstChild, 460, 460);

  };
}

async function wrapText(ctx, text, x, y, maxWidth, fontSize) {
  let line = '';
  let lineHeight = fontSize + 5; // Adjust for line spacing

  for (let i = 0; i < text.length; i++) {
      const testLine = line + text[i];
      const metrics = ctx.measureText(testLine);
      const testWidth = metrics.width;

      if (testWidth > maxWidth && i > 0) {
          ctx.fillText(line, x, y);
          line = text[i];
          y += lineHeight;
      } else {
          line = testLine;
      }
  }

  ctx.fillText(line, x, y);
}


async function downloadImage() {
      // addTextToImage()
      const canvas = document.getElementById('imageCanvas');
      const dataUrl = canvas.toDataURL('image/png');
      
      const a = document.createElement('a');
      a.href = dataUrl;
      a.download = removeRightPartAfterLastDot(document.getElementById("doc-file").files[0].name)+" Updated.png";
      a.click();
  }


 function  addFileInfo(){
  const fileInput = document.getElementById('doc-file');
  const file = fileInput.files[0];

        // Check the file type based on the extension
        const fileType = getFileType(file.name);

        // Process the file based on its type
        if (fileType === 'image') {
          downloadImage()
        } else if (fileType === 'pdf') {
          addTextToPDF()
        } else {
            alert('Unsupported file type. Please select an image (jpg, jpeg, png) or a PDF.');
        }
    }

  
    function getFileType(fileName) {
      const extension = fileName.split('.').pop().toLowerCase();
      if (['jpg', 'jpeg', 'png'].includes(extension)) {
          return 'image';
      } else if (extension === 'pdf') {
          return 'pdf';
      } else {
          return 'unknown';
      }
  }



  function refreshPage() {
    location.reload(); // This will reload the current page
  }

// function displaySelectedImage() {

//   $("#uploaded-file-hash").html(
//     `<h5 class="file-hash-code">File hash: ${fileHash}</h5>`
    
//   );
//   $("#uploaded-file-url").html(
//     `<h5 class="file-url">File URL: ${fileUrl}</h5>`
//   );
//   var fileInput = document.getElementById("doc-file");
//   var selectedImage = document.getElementById("uploaded-image");

//   // Check if a file is selected
//   if (fileInput.files.length > 0) {
//     var file = fileInput.files[0];

//     // Use FileReader to read the selected file as a data URL
//     var reader = new FileReader();

//     reader.onload = function (e) {
//       // Set the source of the <img> element to the data URL
//       selectedImage.src = e.target.result;
//     };

//     // Read the file as a data URL
//     reader.readAsDataURL(file);
//   } else {
//     // Clear the image if no file is selected
//     selectedImage.src = "";
//   }

//   document.getElementById("qrcode_final").innerHTML = "";
//   console.log("making qr-code...");
//   var qrcode = new QRCode(document.getElementById("qrcode_final"), {
//     colorDark: "#000",
//     colorLight: "#fff",
//     correctLevel: QRCode.CorrectLevel.H,
//   });
//   if (!window.hashedfile) return;
//   let url = `${window.location.host}/verify.html?hash=${window.hashedfile}`;
//   // let url = `https://ipfs.io/ipfs/${fileCID}`;
//   qrcode.makeCode(url);
// }

  
// function downloadCertificate() {
//   const certificate = document.getElementById('convert-to-image');

//   // Convert the certificate div to an image
//   html2canvas(certificate).then(function(canvas) {
//     // Create a download link
//     const downloadLink = document.createElement('a');
//     downloadLink.href = canvas.toDataURL('image/png');
//     downloadLink.download = 'QualitestDocument.png';

//     // Append the link to the document and trigger a click to start the download
//     document.body.appendChild(downloadLink);
//     downloadLink.click();
//     document.body.removeChild(downloadLink);
//   });
// }


