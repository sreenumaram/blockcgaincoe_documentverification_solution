// you can JWT (Secret access token) by creating API key in pinata cloud

const JWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI4YTFkNzMzZS1kNmI4LTRmZWItYmZjYi02Y2ZjYmViZmYxM2YiLCJlbWFpbCI6InNyZWVuaXZhc2EubWFyYW1AcXVhbGl0ZXN0Z3JvdXAuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBpbl9wb2xpY3kiOnsicmVnaW9ucyI6W3siaWQiOiJGUkExIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9LHsiaWQiOiJOWUMxIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9XSwidmVyc2lvbiI6MX0sIm1mYV9lbmFibGVkIjpmYWxzZSwic3RhdHVzIjoiQUNUSVZFIn0sImF1dGhlbnRpY2F0aW9uVHlwZSI6InNjb3BlZEtleSIsInNjb3BlZEtleUtleSI6ImY4NmRkZjhiZTU5YzZmNDlkNjA2Iiwic2NvcGVkS2V5U2VjcmV0IjoiZTk0OTYwMTA4ZGVjMzIwZDI5YzE5NzZlYmU3YzJiZGRlYjA2N2NlNzlkMGRmZDQxOTQyYTE4NzU0ZDhjMDU2ZSIsImlhdCI6MTY5NTI3NzcxNn0.1CfOxJPBYnkAx8HYA7yF3FMk6apSjM4DCFOpwhVzIq8'
let objectName;

// async function EnterJWT(){
//     JWT = document.getElementById('textInput').value;
// }

async function UploadFileToIPFS() {
    
const formData = new FormData();
const fileInput = document.getElementById('fileInput'); // Assuming you have an HTML input element for file upload with id="fileInput"
const file = fileInput.files[0];

formData.append('file', file);
objectName = document.getElementById('object_name').value;

const pinataMetadata = JSON.stringify({ name: objectName });
formData.append('pinataMetadata', pinataMetadata);

const pinataOptions = JSON.stringify({ cidVersion: 0 });
formData.append('pinataOptions', pinataOptions);

try {
const res = await fetch("https://api.pinata.cloud/pinning/pinFileToIPFS", {
method: 'POST',
body: formData,
headers: {
'Authorization': `Bearer ${JWT}`
}
});

if (res.ok) {
const data = await res.json();
console.log(data);
const result = JSON.stringify(data);
result.split()
const resultContainer = document.getElementById('result_container');
    resultContainer.textContent = result;
} else {
console.error('Failed to upload file to IPFS:', res.status, res.statusText);
}
} catch (error) {
console.error('Error:', error);
}
}

