window.CONTRACT = {
  // address: "0xb0482BDd7e23daF65D38911eb5D0eFc27bd6d87d",

  // network: "HTTP://127.0.0.1:7545",

  // address: "0xb7836C468bC64C729E873f0FA4191FF4b7b9F09b",

  // polygon mumbai
  // address: "0x00450641a3fbf0E2383101132E48096a9Ac673D8",
  // network: "https://polygon-mumbai.g.alchemy.com/v2/WezGAtSX0lq3AhSP8lIRIiftaCYBR3YR",

  
  network: "https://a0mtczpsjd:tFnGKGcYMzhK8Wl9-9L0QupSf5uLW2hlpFTtLDa-0Tk@a0e6abbjr2-a0w67afc0g-rpc.au0-aws.kaleido.io/",
  address: "0xfc9AEb0a17Fd07da392DE06Ad375f84E521D5958",

  explore: "https://mumbai.polygonscan.com/",
  
  abi:[
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "sender",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "bytes32",
          "name": "hash",
          "type": "bytes32"
        }
      ],
      "name": "RequestViewDocConfirmation",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "_exporter",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "string",
          "name": "_ipfsHash",
          "type": "string"
        }
      ],
      "name": "addHash",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "hash",
          "type": "bytes32"
        },
        {
          "internalType": "string",
          "name": "_ipfs",
          "type": "string"
        }
      ],
      "name": "addDocHash",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_info",
          "type": "string"
        }
      ],
      "name": "add_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_newInfo",
          "type": "string"
        }
      ],
      "name": "alter_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_newOwner",
          "type": "address"
        }
      ],
      "name": "changeOwner",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "count_Exporters",
      "outputs": [
        {
          "internalType": "uint16",
          "name": "",
          "type": "uint16"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "count_hashes",
      "outputs": [
        {
          "internalType": "uint16",
          "name": "",
          "type": "uint16"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "_hash",
          "type": "bytes32"
        }
      ],
      "name": "deleteHash",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        }
      ],
      "name": "delete_Exporter",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes32",
          "name": "_hash",
          "type": "bytes32"
        }
      ],
      "name": "findDocHash",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_add",
          "type": "address"
        }
      ],
      "name": "getExporterInfo",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "owner",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    }
  ]
};
// const web3 = new Web3(new Web3.providers.HttpProvider(window.CONTRACT.network));
const web3 = new Web3(new Web3.providers.WebsocketProvider("wss://a0mtczpsjd:tFnGKGcYMzhK8Wl9-9L0QupSf5uLW2hlpFTtLDa-0Tk@a0e6abbjr2-a0w67afc0g-wss.au0-aws.kaleido.io/"));
const contract = new web3.eth.Contract(
  window.CONTRACT.abi,
  window.CONTRACT.address
);

window.onload = async () => {
  $("#loader").hide();
  $(".loader-wraper").fadeOut("slow");
  checkURL();
  $("#upload_file_button").attr("disabled", true);
  // verifyUsingHash()
};

async function enableVerify(){
  $("#upload_file_button").attr("disabled", false);
}

let enteredHash;
async function verifyUsingHash(){
    let hashInput = document.getElementById("hash-to-verify");
        // Access the value entered by the user
        enteredHash = (hashInput.value).trim();
        console.log(enteredHash+"-----------------"+enteredHash.length);
     
        
        //  if((enteredHash.length) < 66 || (enteredHash.length) > 66 )
        //   {
        //     $("#note").html(
        //     `<h5 class="text-center text-info" id="not_owner"> The hash must be exactly 66 characters long  </h5>`);
            
        //     window.scrollTo({
        //       // top: 100,
        //       top: document.body.scrollHeight,
        //       behavior: 'smooth'
        //   });
        //   $("#verified-result").hide();
        //   return;
        //   }
          // else{
        //     let decimalValue = BigInt(enteredHash);

        // // Convert decimal to string
        // let stringHashValue = decimalValue.toString();
        // let firstTwoChars =  stringHashValue.substring(0,2).toLowerCase();
        //    if(firstTwoChars!= "0x"){

        //     console.log( stringHashValue+"-"+ firstTwoChars);  
        //     $("#note").html(
        //       `<h5 class="text-center text-info" id="not_owner">File Hash should start with prefix 0x </h5>`);
              
        //       window.scrollTo({
        //         // top: 100,
        //         top: document.body.scrollHeight,
        //         behavior: 'smooth'
        //     });

        //     $("#verified-result").hide();
        //   }}
        console.log("QT HASH:"+ enteredHash)
        try{
        await contract.methods
        .findDocHash(enteredHash)
        // .call({ from: window.userAddress })
        .call()
        .then((result) => {
          console.log(result);
          $(".transaction-status").removeClass("d-none");
          window.newHash = result;
      
          if ((result[0] != 0) & (result[1] != 0)) {
            print_info(result, true);
          } else {
            print_info(result, false);
          }
        });
      }
      catch(error){
        console.log(error.message+"verify page");
        print_info(result, false);
      }
  }


// async function verify_Hash() {
//   $("#loader").show();
//   if (window.hashedfile) {
//     /*   I used the contract address as the caller of the function 'findDocHash'
//         you can use any address because it used just for reading info from the contract
//     */
//     await contract.methods
//       .findDocHash(window.hashedfile)
//       .call({ from: window.userAddress })
//       .then((result) => {
//         console.log(result);
//         $(".transaction-status").removeClass("d-none");
//         window.newHash = result;
//         if ((result[0] != 0) & (result[1] != 0)) {
//           print_info(result, true);
//         } else {
//           print_info(result, false);
//         }
//       });
//   }
// }


// async function verify_Hash() {
//   try {
//     let hashInput = document.getElementById("hash-to-verify");
//     // Access the value entered by the user
//     let  enteredHash = hashInput.value;
//     console.log("QT HASH:"+ enteredHash)

//       await contract.methods
//       .findDocHash(window.hashedfile)
//       // .findDocHash(enteredHash)
//       .call({ from: window.userAddress })
//       .then((result) => {
//         console.log(result);
//         $(".transaction-status").removeClass("d-none");
//         window.newHash = result;
//         if(currentExporterInfo=== ""){
//       //   window.info = "Unregistered companies are restricted from verifying files." ;
//         $("#note").html(`<h5 class="text-center" >File is not available in the system😕 </h5>`);
//         $("#download-document").hide();
//     $("#file-hash").html(
//       `<span class="text-info"><i class="fa-solid fa-hashtag"></i></span> ${truncateAddress(
//         window.hashedfile
//       )}`
//     );
//     $("#college-name").hide();
//     $("#contract-address").hide();
//     $("#time-stamps").hide();
//     $("#blockNumber").hide();
//     $(".transaction-status").hide();

//       }
//        else if ((result[0] != 0) & (result[1] != 0)) {
//           print_info(result, true);
//         } else {
//           print_info(result, false);
//         }
//       });
//   } 
//   catch (error) {

//     // console.error("Only owner can verify document 🥲");
//       console.error('Error:', " your error :" + error.message);
//       // if(currentExporterInfo=== ""){
//       //   window.info = "Unregistered companies are restricted from verifying files." ;
//       //   $("#note").html(`<h5 class="text-center">Unregistered companies are restricted from verifying files</h5>`);
//       // }else{
//       $("#note").html(
//         `<h5 class="text-center text-info" id="not_owner">File is not available in the system😕  </h5>`
//       );
//       // }
      
// }
// }





function checkURL() {
  let url_string = window.location.href;
  let url = new URL(url_string);
  window.hashedfile = url.searchParams.get("hash");
  if (!window.hashedfile) return;

  verify_Hash();
}



async function get_Sha3() {
  $("#note").html(`<h5 class="text-warning">Hashing Your File 😴...</h5>`);
  $("#upload_file_button").attr("disabled", false);
  console.log("file changed");
  var file = await document.getElementById("doc-file").files[0];
  if (file) {
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = async function (evt) {
      // var SHA256 = new Hashes.SHA256();
      // = SHA256.hex(evt.target.result);
      window.hashedfile = await web3.utils.soliditySha3(evt.target.result);
      console.log(`File Hash : ${window.hashedfile}`);
      $("#note").html(
        `<h5 class="text-center text-info"> 😎 </h5>`
      );
    };
    reader.onerror = function (evt) {
      console.log("error reading file");
      return false;
    };
  } else {
    window.hashedfile = null;
    return false;
  }
}

function print_info(result, is_verified) {
  //Default Image for not Verified Docunets
  // document.getElementById("student-document").src = "./files/notvalid.svg";
  document.getElementById("student-document").src="assets/images/sample red cross mark.png"
  $("#loader").hide();

  // when document not verfied
   if(!is_verified){
    if((enteredHash.length) < 66 || (enteredHash.length) > 66){
      
      $("#verified-result").hide();
      $("#note").html(
        `<h5 class="text-center text-info" id="not_owner"> The hash must be exactly 66 characters long  </h5>`);
        
        window.scrollTo({
          // top: 100,
          top: document.body.scrollHeight,
          behavior: 'smooth'
      });
    }else
    {
    // document.getElementById('download-document').classList.add('d-none')
    $("#download-document").hide();
    $("#not_owner").hide();
    $("#doc-status").html(`<h3 class="text-danger">
        File not Verified 😕
         <i class="text-danger  fa fa-times-circle" aria-hidden="true"></i>
        </h3>`);
    // $("#file-hash").html(
    //   `<span class="text-info"><i class="fa-solid fa-hashtag"></i></span> ${truncateAddress(
    //     window.hashedfile
    //   )}`
    // );
    $("#college-name").hide();
    $("#contract-address").hide();
    $("#time-stamps").hide();
    $("#blockNumber").hide();
    $(".transaction-status").show();
    window.scrollTo({
      // top: 100,
      top: document.body.scrollHeight,
      behavior: 'smooth'
  });
}
  }
  else {
    $("#download-document").show();
    // when document verfied
    $("#college-name").show();
    $("#contract-address").show();
    $("#time-stamps").show();
    $("#blockNumber").show();

    var t = new Date(1970, 0, 1);
    t.setSeconds(result[1]);
    console.log(result[1]);
    t.setHours(t.getHours() + 3);
    // hide loader
    $("#loader").hide();
   
    $("#doc-status").html(`<h3 class="text-info">
         File Verified Successfully 😊
         <i class="text-info fa fa-check-circle" aria-hidden="true"></i>
        </h3>`);

        $("#not_owner").hide();
    // $("#file-hash").html(
    //   `<span class="text-info"><i class="fa-solid fa-hashtag"></i></span> ${truncateAddress(
    //     window.hashedfile
    //   )}`
    // );
    // $("#college-name").html(
    //   `<span class="text-info"><i class="fa-solid fa-graduation-cap"></i></span> ${result[2]}`
    // );
    // $("#contract-address").html(
    //   `<span class="text-info"><i class="fa-solid fa-file-contract"></i> </span>${truncateAddress(
    //     window.CONTRACT.address
    //   )}`
    // );
    // $("#time-stamps").html(
    //   `<span class="text-info"><i class="fa-solid fa-clock"></i> </span>${t}`
    // );
    // $("#blockNumber").html(
    //   `<span class="text-info"><i class="fa-solid fa-cube"></i></span> ${result[0]}`
    // );
    document.getElementById("student-document").src = "assets/images/GreenTick.png"
      // "https://ipfs.io/ipfs/" + result[3];
    // "https://ipfs.filebase.io/ipfs/" + result[3];
    document.getElementById("download-document").href = "https://ipfs.io/ipfs/" + result[3];
      // document.getElementById("student-document").src;
    $(".transaction-status").show();
    window.scrollTo({
      // top: 100,
      top: document.body.scrollHeight,
      behavior: 'smooth'
  });
  }
}

function truncateAddress(address) {
  if (!address) {
    return;
  }
  return `${address.substr(0, 7)}...${address.substr(
    address.length - 8,
    address.length
  )}`;
}
